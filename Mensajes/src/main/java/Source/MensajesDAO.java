package Source;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author reek
 */
public class MensajesDAO {

    public static void crearMensaje(Mensaje mensaje) {
        Conexion dbConect = new Conexion();
        try (Connection conexion = dbConect.getConexion()) {
            PreparedStatement ps = null;
            try {
                String query = "INSERT INTO `mensajes_app`.`mensajetb` (`MENSAJE`, `IDAUTOR`, `FECHA` ) VALUES (?,?,?);";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.getAutor());
                ps.setString(3, mensaje.getFecha());
                ps.executeUpdate();
                System.out.println("Mensaje creado");
            } catch (SQLException e) {
                System.out.println("No se pudo insertar el mensaje");
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void leerMensaje() {
        Conexion dbConect = new Conexion();
        try (Connection conexion = dbConect.getConexion()) {
            PreparedStatement ps = null;
            ResultSet rs = null;

            try {
                String query = "SELECT * FROM mensajetb";

                ps = conexion.prepareStatement(query);
                rs = ps.executeQuery();

                while (rs.next()) {
                    Mensaje mensaje = new Mensaje();
                    mensaje.setIdmensaje(rs.getInt("IDMENSAJE"));
                    mensaje.setAutor(rs.getString("IDAUTOR"));
                    mensaje.setMensaje(rs.getString("MENSAJE"));
                    mensaje.setFecha(rs.getString("FECHA"));
                    mensaje.leerMensaje();
                }

            } catch (Exception e) {
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    public static void borrarMensaje(int idmensaje) {

    }

    public static void actualizarMensaje(Mensaje mensaje) {

    }
}
