package Source;

import java.sql.Connection;
import java.util.Scanner;

/**
 *
 * @author reek
 */
public class Inicio {

    public static void main(String[] args) {
        Conexion con = new Conexion();
        Scanner scan = new Scanner(System.in);
        int opcion = 0;

        do {
            System.out.println("---------------------");
            System.out.println("Aplicacion de mensajes");
            System.out.println("1.Crear un mensaje");
            System.out.println("2.Listar mensajes");
            System.out.println("3.Editar mensaje");
            System.out.println("4.Eliminar mensaje");
            System.out.println("5.Salir");
            //Leer opcin del usuario
            opcion = scan.nextInt();

            switch (opcion) {
                case 1:
                    MensajesService.crearMensaje();
                    break;
                case 2:
                    MensajesService.listarMensaje();
                    break;
                case 3:
                    MensajesService.editarMensaje();
                    break;
                case 4:
                    MensajesService.borrarMensaje();
                    break;
                default:
                    opcion = 5;
                    break;

            }

        } while (opcion != 5);

    }

}
