package Source;

/**
 *
 * @author reek
 */
public class Mensaje {

    int idmensaje;
    String mensaje;
    String autor;
    String fecha;

    public Mensaje() {
    }

    public Mensaje(String mensaje, String autor, String fecha) {
        this.mensaje = mensaje;
        this.autor = autor;
        this.fecha = fecha;
    }

    public int getIdmensaje() {
        return idmensaje;
    }

    public void setIdmensaje(int idmensaje) {
        this.idmensaje = idmensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void leerMensaje() {
        System.out.println("-------------------------");
        System.out.println("ID: " + this.idmensaje);
        System.out.println("Mensaje: " + this.mensaje);
        System.out.println("Autor: " + this.autor);
        System.out.println("Fecha: " + this.fecha);
        System.out.println("-------------------------\n");
    }

}
