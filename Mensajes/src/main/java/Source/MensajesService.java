package Source;

import java.util.Scanner;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;

/**
 *
 * @author reek
 */
public class MensajesService {

    public static void crearMensaje() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Escribe tu mensaje");
        String men = scan.nextLine();

        System.out.println("Escribe tu nombre");
        String nombre = scan.nextLine();

        Date fecha = new Date();

        Mensaje mensaje = new Mensaje(men, nombre, fecha.toString());
        MensajesDAO.crearMensaje(mensaje);

    }

    public static void listarMensaje() {
        MensajesDAO.leerMensaje();

    }

    public static void borrarMensaje() {

    }

    public static void editarMensaje() {

    }
}
