
package Source;

import com.mysql.cj.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author reek
 */
public class Conexion {
    public Connection getConexion(){
        Connection conexion = null;
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/mensajes_app","root","");
            if(conexion != null){
                System.out.println("Conexion establecida");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return conexion;
    }
    
}
